#include<iostream>
#include<cmath>
using namespace std;

int main()
{
	double x1, x2, x3, D, a, b, c;

	cout << "Vid uravneniya: y=ax^2+bx+c" << endl;
	cout << "Type a,b,c" << endl;
	cin >> a >> b >> c;

	D = (b*b) - (4 * a*c);
	
	if (D < 0)
	{
		cout << "error\n" << endl;
	}
	else
	{
		x1 = (-b - sqrt(D)) / (2 * a);
		x2 = (-b + sqrt(D)) / (2 * a);

	cout << "x1=" << x1 << endl;
	cout << "x2=" << x2 << endl;
	}


	system("pause");
	return 0;
}